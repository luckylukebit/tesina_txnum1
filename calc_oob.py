#!/usr/bin/env python

import scipy
import numpy

from calc_s import DensCPM
from calc_cpfsk import DensCPFSK
from common import *


N=500
P=0.99
if __name__=='__main__':
    minf = 0
    maxf = 4/T
    if options.modo == 'default':
        if (L == 1 and options.g == 'rec'):
            tc = DensCPFSK()
        else:
            #tc =DensEq()
            tc =DensCPM()
    elif options.modo == 'cpm':
        #tc =DensEq()
        tc =DensCPM()
    elif options.modo == 'cpfsk':
        tc = DensCPFSK()
    x = numpy.linspace(minf, maxf, N)
    fn = tc.S
    f = numpy.frompyfunc(fn, 1, 1)
    y = f(x)/T
    flim = y[-1]
    tot = scipy.integrate.simps(y, x)
    print
    print 'Semi-integrale:', tot
    for n in range(5, N):
        xx = x[:n]
        yy = y[:n]
        ints = scipy.integrate.simps(yy, xx)
        if scipy.real(ints) >= tot*P:
            flim = x[n-1]
            break
    # flim e' monolatera
    print '[da %d  %d] %.0f%% della potenza in %f fT' %(minf*T, maxf*T, P*100, flim*T*2)
    Eb = scipy.log2(M)/(T*(flim*2))
    print 'Efficienza spettrale: %f bit/s/Hz' %Eb
