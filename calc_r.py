#!/usr/bin/env python

import os
import sys

from scipy import pi, exp
import scipy
import numpy

#from plot import getsymbols, q, make_func_s, P, cplx_integral
from common import *

class Autocorr(FileFunc):
    def prod_el(self, t, k, tau):
        val = scipy.sin(2*pi*h*M*(q(t+tau-k*T)-q(t-k*T)))/\
            (M*scipy.sin(2*pi*h*(q(t+tau-k*T)-q(t-k*T))))
        #if numpy.isnan(val):
            # limite per tau->0
            # sfrutto sen(2x)=2sen(x)cos(x) e il fatto che M e' una potenza di 2
        #    val = scipy.prod([scipy.cos(2*pi*h*(2**(i-1))*(q(t+tau-k*T)-q(t-k*T)))
        #               for i in range(1, int(scipy.log2(M)+1))])

        #val = sum([2*scipy.cos(2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))  for n in [tmp for tmp in getsymbols() if tmp>0]])/M

        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val
    def prod_el1(self, t, k, tau):
        val = sum([2*scipy.cos(2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))  for n in [tmp for tmp in getsymbols() if tmp>0]])/M

        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val
    def prod_el2(self, t, k, tau):
        m = int(tau/T)
        xi = tau-m*T
        val = sum([P[n]*exp(1j*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])

        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val

    def innerprod(self, t, tau):
        # simboli equiprobabili
        #return scipy.prod([self.prod_el1(t, k, tau) for k in range(1-int(L), int(tau/T)+2)])
        # generale
        return scipy.prod([self.prod_el2(t, k, tau) for k in range(1-int(L), int(tau/T)+2)])
    def R(self, tau):
        #return cplx_integral(innerprod, 0, T)/(2*T)
        #xvr, yvr = make_func_s(lambda t: self.innerprod(t, tau), 0, T, 50)
        #val = scipy.integrate.simps(yvr, xvr)/(2*T)
        #val = cplx_integral(lambda t: self.innerprod(t, tau), 0, T)/(2*T)
        val = simps_integral(lambda t: self.innerprod(t, tau), 0, T, R_INT_N)/(2*T)
        #val = simps_interp_integral(lambda t: self.innerprod(t, tau), 0, T, R_INT_N)/(2*T)
        if numpy.isnan(val):
            print tau, val
        return val

class Autocorr_v(FileFunc):
    def __init__(self):
        self.prod_el = numpy.frompyfunc(self._prod_el, 3, 1)
        self.prod_el1 = numpy.frompyfunc(self._prod_el1, 3, 1)
        self.prod_el2 = numpy.frompyfunc(self._prod_el2, 3, 1)
        self.innerprod = numpy.frompyfunc(self._innerprod, 2, 1)
        self.R = numpy.frompyfunc(self._R, 1, 1)

    def _prod_el(self, t, k, tau):
        val = scipy.sin(2*pi*h*M*(q(t+tau-k*T)-q(t-k*T)))/\
            (M*scipy.sin(2*pi*h*(q(t+tau-k*T)-q(t-k*T))))
        #if numpy.isnan(val):
            # limite per tau->0
            # sfrutto sen(2x)=2sen(x)cos(x) e il fatto che M e' una potenza di 2
        #    val = scipy.prod([scipy.cos(2*pi*h*(2**(i-1))*(q(t+tau-k*T)-q(t-k*T)))
        #               for i in range(1, int(scipy.log2(M)+1))])

        #val = sum([2*scipy.cos(2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))  for n in [tmp for tmp in getsymbols() if tmp>0]])/M

        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val

    def _prod_el1(self, t, k, tau):
        val = scipy.sum([2*scipy.cos(2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))  for n in [tmp for tmp in getsymbols() if tmp>0]])/M

        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val

    def _prod_el2(self, t, k, tau):
        m = scipy.floor(tau/T)
        xi = tau-m*T
        val = scipy.sum([P[n]*exp(1j*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])
        #n = numpy.array(getsymbols())
        n = numpy.arange(M)[1::2]
        #val = scipy.sum(exp(1j*2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))/M)
        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val

    def _innerprod(self, t, tau):
        k = numpy.arange(1-L, tau/T+1).reshape((-1, 1))
        return scipy.prod(self.prod_el2(t, k, tau))

    def _R(self, tau):
        #if (tau % (T*50/S_INT_N)) == 0.0:
        if True:
            print '.',
            sys.stdout.flush()
        def _tauprod(t):
            return self.innerprod(t, tau)
        tauprod = numpy.frompyfunc(_tauprod, 1, 1)
        val = simps_integral(tauprod, 0, T, R_INT_N)/(2*T)
        if numpy.isnan(val):
            print tau, val
        return val

def calc_r(tau):
    # dimensioni: tau, t, k, n
    #`            0    1  2  3

    tmp_s_int = tau.shape[0]

    n = numpy.arange(-M, M)[1::2].reshape((1, 1, 1, -1))
    Pn = numpy.array([P[_n] for _n in n.flatten() ]).reshape(1, 1, 1, -1)
    #val = scipy.sum(exp(1j*2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))/M)
    k = numpy.arange(1-L, tau.max()/T+1).reshape((1, 1, -1, 1))
    #k = numpy.arange(1-L, 1.2).reshape((1, 1, -1, 1))
    #tm = tau.max()
    #kb = [tau.max()/T, 1-L]
    # mmm se voglio calcolarla bilatera devo trovare il range giusto della sommatoria
    #k = numpy.arange(-10, 10).reshape((1, 1, -1, 1))
    t = numpy.linspace(0, T, R_INT_N).astype('float128').reshape((1, -1, 1, 1))
    #t = numpy.linspace(0, T, R_INT_N).reshape((1, -1)).repeat(S_INT_N, axis=0).reshape((S_INT_N, R_INT_N))
    tau = tau.reshape((-1, 1, 1, 1))

    numpy.setbufsize(10e6)
    inexp = 1j*2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)).astype('float128')
    s = scipy.sum(Pn*scipy.exp(inexp), axis=3)
    p = scipy.prod(s, axis=2)
    #t = t.reshape((1, -1)).repeat(S_INT_N, axis=0)
    t = t.reshape((1, -1)).repeat(tmp_s_int, axis=0)
    y = scipy.integrate.simps(p, t, axis=1)

    return y/(2*T)

def calc_r_big(tau):
    nsplit = S_INT_N/50
    subarrays = numpy.split(tau, nsplit)
    subtots = []
    for sa in subarrays:
        print '.',
        sys.stdout.flush()
        st = calc_r(sa)
        subtots.append(st)
    return numpy.concatenate(subtots)

if __name__=='__main__':
    xlim = [0, L*T]
    xlim1 = [L*T, (L+1)*T]

    tc =Autocorr()
    x = numpy.linspace(xlim[0], xlim[1], S_INT_N)
    x = x.astype('float128')
    x1 = numpy.linspace(xlim1[0], xlim1[1], S_INT_N)
    x1 = x1.astype('float128')

    # y, y1 = [], []
    # for n, _x in enumerate(x):
    #     if n%(S_INT_N/50) == 0:
    #         print '.',
    #         sys.stdout.flush()
    #     y.append(tc.R(_x))
    # for n, _x in enumerate(x1):
    #     if n%(S_INT_N/50) == 0:
    #         print '.',
    #         sys.stdout.flush()
    #     y1.append(tc.R(_x))

    #y = calc_r(x)
    #y1 = calc_r(x1)
    y = calc_r_big(x)
    y1 = calc_r_big(x1)

    # fn = numpy.frompyfunc(tc.R, 1, 1)
    # y = fn(x)
    # y1 = fn(x1)

    #y = tc.R(x)
    #y1 = tc.R(x1)

    tc.save_file(x, y, 'R')
    tc.save_file(x1, y1, 'R1')
