#!/usr/bin/env python

import numpy
import scipy

from matplotlib import pyplot
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.ticker as ticker

import common

if __name__=='__main__':
    minx = -1
    maxx = common.L+1
    x = numpy.linspace(minx, maxx, 500)
    #y = [common.g_REC(t) for t in x]
    #y = [q(t), g_REC) for t in x]
    #y = [common.g_RC(t) for t in x]
    #y = [q(t) for t in x]
    y = common.q(x)
    xlim = [minx, maxx]
    ymax = max(scipy.real(y))
    ylim = [0, ymax*1.5]
    #ymax_RC = max(scipy.real(y3))
    #ylim_RC = [-0.05, ymax_RC*1.5]

    fig = pyplot.figure(1)

    # grafico g REC
    ax = SubplotZero(fig, 111, ylim=ylim, xlim=xlim, autoscale_on=False)
    fig.add_subplot(ax)

    opt_line = ax.plot(x/common.T, y, 'k')

    ax.axis['xzero'].set_visible(True)
    ax.axis['yzero'].set_visible(True)
    formatter = ticker.FormatStrFormatter('%1.1fT ')
    ax.xaxis.set_major_formatter(formatter)
    ax.set_yticks([ymax, ylim[1]-0.0001])
    ax.set_yticklabels(['$\\frac{1}{2}$', '$q(t)$'])
    #ax.set_xlabel('(d)')
    for direction in ["left", "right", "bottom", "top"]:
        ax.axis[direction].set_visible(False)

    #pyplot.grid(linestyle='-', color='r', visible=True)

    fig.savefig('%s.eps' %common.options.output_file)
    #pyplot.show()
