#!/usr/bin/env python

from mpl_toolkits.mplot3d.axes3d import Axes3D
import matplotlib.pyplot as plt

from matplotlib import pyplot
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.ticker as ticker

# imports specific to the plots in this example
import numpy as np
from matplotlib import cm
# from mpl_toolkits.mplot3d.axes3d import get_test_data

# # Twice as wide as it is tall.
# fig = plt.figure(figsize=plt.figaspect(0.5))

# #---- First subplot
# ax = fig.add_subplot(1, 2, 1)
# X = np.arange(-5, 5, 0.25)
# Y = np.arange(-5, 5, 0.25)
# X, Y = np.meshgrid(X, Y)
# R = np.sqrt(X**2 + Y**2)
# Z = np.sin(R)
# surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,
#         linewidth=0, antialiased=False)
# ax.set_zlim3d(-1.01, 1.01)

# fig.colorbar(surf, shrink=0.5, aspect=10)

# #---- Second subplot
# ax = fig.add_subplot(1, 2, 2)
# X, Y, Z = get_test_data(0.05)
# ax.plot_wireframe(X, Y, Z, rstride=10, cstride=10)

# plt.show()

# import mpl_toolkits.mplot3d.axes3d as p3

# # this connects each of the points with lines
# fig=plt.figure()
# ax = p3.Axes3D(fig)
# # plot3D requires a 1D array for x, y, and z
# # ravel() converts the 100x100 array into a 1x10000 array

# from numpy import *

# t = linspace(0, 10, 100)
# #tau = linspace(0, 10, 100)
# k = linspace(0, 10, 100)

# T=0.5

# # t+tau-kT=0.5
# tau = 0.5-t+k*T

# print t, tau, k

# ax.plot3D(t, tau, k)
# #ax.plot_wireframe(t, tau, k)
# #surf = ax.plot_surface(t, tau, k, rstride=1, cstride=1,
# #        linewidth=0, antialiased=False)
# ax.set_xlabel('t')
# ax.set_ylabel('tau')
# ax.set_zlabel('k')
# fig.add_axes(ax)
# plt.show()


import common
import matplotlib.pyplot as plt

import scipy
import numpy
import sys

from scipy import pi, cos, sin, exp
import scipy.interpolate

k=-1
#tau=float(sys.argv[1])
tau=0
n=1

from common import q, g_RC

def _fi(t):
    return q(t)
    #return g_RC(t)
    #print t, q(t+tau-k*T), q(t-k*T)
    #incos = 2*pi*h*n*(q(t+tau-k*T)-q(t-k*T))
    if t <= L*T:
        return (1/(2*L*T))*(t-((L*T)/(2*pi))*scipy.sin((2*pi*t)/(L*T)))
        #return (1/(2*L*T))*(t+((L*T)/(2*pi))*scipy.sin((2*pi*t)/(L*T)))
        #incos = (1/(2*L*T))*(1-scipy.cos((2*pi*t)/(L*T)))
        #return incos
        #return (1/(2*L*T))*(t+((L*T)/(2*pi))*scipy.sin((2*pi*t)/(L*T)))
    else:
        return 0.5
    #return scipy.cos(incos)

def load_mod(m=2.0, h=0.5, l=1.0, g='rec', d='databig'):
    a0 = sys.argv[0]
    new_argv = [a0, '-M', str(m), '-H', str(h), '-L', str(l), '-g', g, '-d', str(d)]
    sys.argv = new_argv
    reload(common)

#fi = numpy.frompyfunc(_fi, 1, 1)

#x = numpy.linspace(0, 2*L*T, 300)
#y = fi(x)
#y = map(_fi, x)
#print x, y
#plot_test(x, y)

def genmulcosf(x, y):
    def intf(f):
        print '.',
        sys.stdout.flush()
        # for i, _x in enumerate(x):
        #     #y[i] = y[i]*cos(s*pi*f*_x)
        #     y[i] = y[i]*exp(-1j*2*pi*f*_x)
        # return y
        return y*exp(-1j*2*pi*f*x)
    return numpy.frompyfunc(intf, 1, 1)

def cplx_simps(x, y):
    rint = scipy.integrate.simps(scipy.real(y), x)
    iint = scipy.integrate.simps(scipy.imag(y), x)
    return complex(rint, iint)

def calc_s(rx, ry, rx1, ry1):
    print '-'
    xf = scipy.linspace(0, 20, 400)
    psi =  sum([common.P[n]*exp(1j*pi*common.h*n) for n in common.getsymbols()])
    yf = []
    yfunc = genmulcosf(rx, ry)
    yfunc1 = genmulcosf(rx1, ry1)
    for f in xf:
        int1 = scipy.integrate.simps(yfunc(f), rx)
        int2 = (1/(1-psi*exp(-1j*2*pi*f*common.T)))*scipy.integrate.simps(yfunc1(f), rx1)
        #int1 = cplx_simps(rx, yfunc(f))
        #int2 = (1/(1-psi*exp(-1j*2*pi*f*common.T)))*cplx_simps(rx1, yfunc1(f))
        yf.append(2*scipy.real(int1+int2))
    return xf*common.T, numpy.array(yf)

def calc_intcos(x, y, xx, yy):
    f = 0.1
    #x, y = xx, yy
    yfu = genmulcosf(x, y)
    #xf = scipy.linspace(0, 2, 200)
    yf = yfu(f)
    #print len(xf), len(yf)
    #print yf
    return x, yf

#calc_s = calc_intcos

load_mod(g='rec')
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrec = numpy.concatenate((x, x1))
yrec = numpy.concatenate((y, y1))
xrec, yrec = calc_s(x, y, x1, y1)


load_mod(g='rec', l=2, d='data')
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrec2 = numpy.concatenate((x, x1))
yrec2 = numpy.concatenate((y, y1))
xrec2, yrec2 = calc_s(x, y, x1, y1)

load_mod(g='rec', l=3, d='data')
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrec3 = numpy.concatenate((x, x1))
yrec3 = numpy.concatenate((y, y1))
xrec3, yrec3 = calc_s(x, y, x1, y1)

load_mod(g='rc')
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrc = numpy.concatenate((x, x1))
yrc = numpy.concatenate((y, y1))
xrc, yrc = calc_s(x, y, x1, y1)

load_mod(g='rc', l=2)
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrc2 = numpy.concatenate((x, x1))
yrc2 = numpy.concatenate((y, y1))
xrc2, yrc2 = calc_s(x, y, x1, y1)

load_mod(g='rc', l=3)
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrc3 = numpy.concatenate((x, x1))
yrc3 = numpy.concatenate((y, y1))
xrc3, yrc3 = calc_s(x, y, x1, y1)

load_mod(g='rc', l=4)
x, y = common.FileFunc().read_file('R')
x1, y1 = common.FileFunc().read_file('R1')
xrc4 = numpy.concatenate((x, x1))
yrc4 = numpy.concatenate((y, y1))
xrc4, yrc4 = calc_s(x, y, x1, y1)

# frc2 = scipy.interpolate.interp1d(xrc2, yrc2, kind=9)
# xirc2 = scipy.linspace(0, (common.L+1)*common.T, 1000)
# yirc2 = frc2(xirc2)

fig = pyplot.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(xrec, yrec, 'k', xrec2, yrec2, 'k--', xrec3, yrec3, 'k', xrc, yrc, 'b', xrc2, yrc2, 'r', xrc3, yrc3, 'y')
#ax.plot( xrec2, yrec2, 'k--', xrec3, yrec3, 'k', xrc2, yrc2, 'r', xrc3, yrc3, 'y')
#ax.plot(xrc2, yrc2, 'r', xirc2, yirc2, 'y')
#ax.set_yscale('log', nonposy='clip')
ax.set_yscale('log')
#pyplot.xlabel('$fT$')
#pyplot.ylabel('$Densit\`a \, Spettrale \, (dB)$')
fig.savefig('test.eps')
