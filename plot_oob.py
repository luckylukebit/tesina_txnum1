#!/usr/bin/env python

import sys
import optparse
from matplotlib import pyplot
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.ticker as ticker
import scipy

from common import *
from calc_s import DensEq, DensCPM
from calc_cpfsk import DensCPFSK
import numpy

if __name__=='__main__':
    # F = FileFunc()
    # res = F.read_file(options.input_file)
    # if res:
    #     x, y = res
    #     x, y = x, y/T
    # else:
    #     print 'manca il file'
    #     sys.exit(1)

    n=10
    minf=0 #-n/T
    maxf=n/T  # fT=n

    if L == 1 and options.g == 'rec':
        tc = DensCPFSK()
    else:
        #tc =DensEq()
        tc =DensCPM()
    x = numpy.linspace(minf, maxf, 200)
    fn = tc.S
    #f = numpy.frompyfunc(fn, 1, 1)
    #y = f(x)
    y = map(fn, x)

    Tb = T/scipy.log2(M)
    x = x*Tb
    #ref = y[0]
    #y = y/ref


    #for i, yy in enumerate(y):
    #    if yy < 0:
    #        y[i] = -yy

    fig = pyplot.figure()
    #ax = fig.add_subplot(1, 1, 1, xlim=(min(x), max(x)), ylim=(min(y)*1.2, 0), autoscale_on=False)
    ax = fig.add_subplot(1, 1, 1)
    #p = fig.add_subplot(1, 1, 1)
    #noopt_line = p.plot(loads, busy_noopt, 'r')
    y = 10*scipy.log10(y)
    line = ax.plot(x, y, 'b')
    #line = ax.semilogy(x, y, 'b')
    pyplot.xlabel('$fT$')
    pyplot.ylabel('$Densit\`a \, Spettrale$')
    #ax.set_yscale('log', nonposy='clip')
    #help(ax)
    ax.grid(True)
    #pyplot.legend((line,), ('int: %.2f' %ints,))
    fig.savefig('%s.eps' %options.output_file)
    #pyplot.show()
