#!/usr/bin/env python

import sys
import scipy
from scipy import pi

from common import *

class DensCPFSK(FileFunc):
    phi = scipy.sin(M*pi*h)/(M*scipy.sin(pi*h))
    def __init__(self):
        assert L==1
        assert options.g=='rec'
    def alfa(self, n, m):
        return pi*h*(m+n-1-M)

    def A(self, n, f):
        arg = pi*(f*T-0.5*h*(2*n -1 -M))
        return (scipy.sin(arg))/arg

    def B(self, n, m, f):
        num = scipy.cos(2*pi*f*T - self.alfa(n, m)) - self.phi*scipy.cos(self.alfa(n, m))
        den = 1 + self.phi**2 -2*self.phi*scipy.cos(2*pi*f*T)
        return num/den

    def S(self, f):
        #print '-',
        #sys.stdout.flush()
        sum1 = sum([self.A(n, f)**2 for n in range(1, int(M)+1)])
        sum2 = sum([sum([self.B(n, m, f)*self.A(n, f)*self.A(m, f) for m in range(1, int(M)+1)]) for n in range(1, int(M)+1)])
        fy = T*((1/M)*sum1 + (2/(M**2))*sum2)
        # mm ?
        fy = fy/2
        return fy

if __name__=='__main__':
    n=max(M/2.0, 2.0)
    minf=0 #-n/T
    maxf=n/T  # fT=n

    tc =DensCPFSK()
    x = scipy.linspace(minf, maxf, 200)
    fn = tc.S
    f = scipy.frompyfunc(fn, 1, 1)
    y = f(x)
    tc.save_file(x, y, 'S')
