#!/usr/bin/env python

import sys
import scipy
import scipy.misc
import scipy.integrate

from common import *
from calc_r import Autocorr

if __name__=='__main__':
    F = FileFunc()
    # print options.input_file
    # res = F.read_file(options.input_file)
    # if res:
    #     x, y = res
    # else:
    #     print 'manca il file'
    #     sys.exit(1)

    # x = scipy.linspace(0, (L+1)*T, 100)
    # maxx, minx = max(x), min(x)
    # n = len(x)
    # step = (maxx-minx)/n
    # #max_dr = max([scipy.misc.derivative(Autocorr().R, xx, dx=step, n=4, order=5) for xx in x])
    # errs = []
    # for xx in x:
    #     tt = scipy.linspace(0, T, n)
    #     y = []
    #     for t in tt:
    #         f = lambda tau: Autocorr().innerprod(t, tau)
    #         yy = f(xx)
    #         y.append(yy)
    #         max_dr = max([scipy.misc.derivative(lambda tau: Autocorr().innerprod(t, tau), xx, dx=step, n=4, order=5) for xx in x])
    #         maxerr = ((step**4)/180)*(maxx-minx)*max_dr
    #         errs.append(maxerr)
    #         #print 'Massimo errore per Simpson t=', t, maxerr
    #     intot = scipy.integrate.simps(y, tt)
    #     me = max(errs)
    #     print intot, me, me/intot
    # # for t in scipy.linspace(0, T, 50):
    # #     max_dr = max([scipy.misc.derivative(lambda tau: Autocorr().innerprod(t, tau), xx, dx=step, n=4, order=5) for xx in x])
    # #     maxerr = ((step**4)/180)*(maxx-minx)*max_dr
    # #     print 'Massimo errore per Simpson t=', t, maxerr
    # #     errs.append(maxerr)
    # # print 'max err tot:', max(errs)

    rxold, ryold = F.read_file('R')
    r1xold, r1yold = F.read_file('R1')
    rxnew, rynew = F.read_file('RR')
    r1xnew, r1ynew = F.read_file('RR1')

    print max(rxold-rxnew)
    print max(r1xold-r1xnew)
    print min(ryold-rynew)
    print min(r1yold-r1ynew)
