#!/usr/bin/env python

from sympy import *


tau, t, h, n, k, T, M, L, m, xi = map(Symbol, ['tau', 't', 'h', 'n', 'k', 'T', 'M', 'L', 'm', 'xi'])

ww = Symbol('ww')

#rect = Lambda(x, (sign(x)+1)/2) - Lambda(x, (sign(x-L*T)+1)/2)

class g(Function):
    nargs = 1
    @classmethod
    def eval(cls, t):
        if 0<= t <= L*T:
            return 1/(2*T)
        else:
            return 0
        #if arg.is_positive:
        #    return S.One
        #if arg.is_negative:
        #    return S.NegativeOne
        #if isinstance(arg, Basic.Mul):
        #    coeff, terms = arg.as_coeff_terms()
        #    if not isinstance(coeff, Basic.One):
        #        return cls(coeff) * cls(Basic.Mul(*terms))

    is_bounded = True

#def q(tau):
#    return integrate(g(t), (t, -oo, tau))

class q(Function):
    nargs = 1
    @classmethod
    def eval(cls, t):
        return integrate(g(t), (t, -oo, tau))

    is_bounded = True

    # m = int(tau/T)
    # xi = tau-m*T
    # def innersum(k, t):
    #    return sum([P[n]*exp(1j*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])
    # def intrr(t):
    #    #return reduce(lambda tmp1, tmp2: tmp1*tmp2, [innersum(k, t)  for k in range(1-L, m+2)], 1)
    #    return scipy.prod([innersum(k, t)  for k in range(1-L, m+2)])
    # ##
    # def intpp(t):
    #     # sembra piu veloce di intrr
    #     return (1/2)*sum([P[n]*exp(1j*2*pi*h*sum([n*(q(t+tau-k*T)-q(t-k*T)) for k in range(-(L+2), L+3)]))  for n in getsymbols()])
    # ##
    # #return (1/(2*T))*cplx_integral(intrr, 0, T)
    # return (1/(T))*cplx_integral(intpp, 0, T)

class R(Function):
    nargs = 1
    @classmethod
    def eval(cls, tau):
        m = floor(tau/T)
        xi = tau-m*T
        return integrate((1/2)*Sum(P[n]*exp(1j*2*pi*h*Sum(n*(q(t+tau-k*T)-q(t-k*T)), (n, -(L+2), L+3))), (k, 0, M)), (t, 0, T))

    # fy = 2*scipy.real(cplx_integral(intff, 0, L*T)+
    #                   cplx_integral(intff, L*T, (L+1)*T)/(1-psi*exp(-1j*2*pi*f*T)))

def RR(tau):
    m = floor(tau/T)
    xi = tau-m*T
    return integrate((1/2)*Sum(P[n]*exp(1j*2*pi*h*Sum(n*(q(t+tau-k*T)-q(t-k*T)), (n, -(L+2), L+3))), (k, 0, M)), (t, 0, T))

integrate(RR, (tau, 0, T))

#sum = Sum((1/M)*exp(I*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))), (n, -M+1, M-1))
#integrate(Product(sum, (k, 1-L, m-1)), (tau, 0, T))
