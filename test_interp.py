#!/usr/bin/python

import scipy
import scipy.interpolate

N=100

f1 = scipy.cos

x = scipy.linspace(0, 10, N)
y = f1(x)
f2 = scipy.interpolate.interp1d(x, y, kind=9)

x = scipy.linspace(0, 10, N**2)
y1 = f1(x)
y2 = f2(x)
diff = y2-y1
print max(diff)
