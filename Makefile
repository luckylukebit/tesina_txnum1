
#SRC="main.tex intro.tex cap1.tex"
BASE=main
MAINSRC=$(BASE).tex
SRC=$(MAINSRC)
#IMAGES=1REC.eps 1RC.eps 1REC_g.eps 1RC_g.eps
#IMAGES=1rec.eps 1rc.eps 1rec_q.eps 1rc_q.eps cpm_*.eps
IMAGES=$(wildcard 1*.eps cpm_*.eps)

OUTNAME=pres
PDF=$(OUTNAME).pdf
PS=$(OUTNAME).ps

#all: pdf ps
all: ps

$(PDF): $(BASE).dvi
	dvipdf $^ $(PDF)
#pdflatex $(MAINSRC) --shell-escape
#pdflatex $(MAINSRC) --shell-escape
pdf: $(PDF)

$(PS): $(BASE).dvi
	dvips -o $(PS) $^
ps: $(PS)

$(BASE).dvi: $(SRC) $(IMAGES)
	latex --halt-on-error $(MAINSRC)
	latex $(MAINSRC)

images: $(IMAGES)

view: $(PS)
	gnome-open $(OUTNAME).ps

show: view

clean:
	rm -f *.aux *.lof *.lot *.log *.toc *.nav *.out *.snm *.vrb
	find . -name '*.pyc' | xargs rm -f

distclean: clean
	rm -f *.pdf *.dvi *.ps
	rm -f *~ \#*\#
	#rm -f *.eps


#%.eps: %.svg
#	convert -verbose  $^ $@
# 	inkscape -z -f $^ -D --export-eps=$@

# %.eps: %.png
# 	inkscape -z -f $^ --export-eps=$@

.PHONY=dvi pdf ps images clean distclean view all show view
