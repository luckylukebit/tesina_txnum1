#!/usr/bin/env python

from matplotlib import pyplot
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.ticker as ticker
import numpy
import scipy
import scipy.integrate
from scipy.interpolate import interp1d
import cmath as math
import random
import sys
import os

import optparse

pi = scipy.pi
exp = scipy.exp

integral = scipy.integrate.quad
#integral = scipy.integrate.simps
#integral = scipy.integrate.quadrature
#integral = scipy.integrate.fixed_quad
# def integral(f, a, b):
#     x = numpy.linspace(a, b, 1000)
#     y = map(f, x)
#     return [scipy.integrate.simps(y, x), None]
# def integral(f, a, b):
#     return [scipy.integrate.romberg(f, a, b), None]
# def integral(f, a, b):
#     n = 100
#     x = numpy.linspace(a, b, n)
#     #y = map(f, x)
#     dx = (a-b)/n
#     return [sum(map(lambda _x: f(_x)*dx, x)), None]

def plot_test(x, y):
    fig = pyplot.figure()
    p = fig.add_subplot(1, 1, 1)
    opt_line = p.plot(x, y, 'b')
    fig.savefig('test.svg')
    import sys
    sys.exit(0)

_momd = {}
def cplx_integral(func, a, b, **kwargs):
    kwargs['limit']=500
    kwargs['weight'] = 'sin'
    kwargs['wvar'] = 2*pi*h*M
    #kwargs['full_output'] = 1
    def real_func(x):
        return scipy.real(func(x))
    def imag_func(x):
        return scipy.imag(func(x))
    real_integral = integral(real_func, a, b, **kwargs)
    #real_d = real_integral[2]
    imag_integral = integral(imag_func, a, b, **kwargs)
    #imag_d = imag_integral[2]
    #_momd[func] = (real_d, imag_d)
    #return (real_integral[0] + 1j*imag_integral[0], real_integral[1:], imag_integral[1:])
    return real_integral[0] + 1j*imag_integral[0]
    #return integral(func, a, b, **kwargs)[0]

def read_file(f):
    # crea una funzione partendo da un file con dei campioni
    xv, yv = [], []
    with open(f) as fh:
        for l in fh.readlines():
            # _xr, _xi, _yr, _yi = l.strip().split('\t')
            # _xr, _xi, _yr, _yi = map(float, [_xr, _xi, _yr, _yi])
            # xv.append(_xr+1j*_xi)
            # yv.append(_yr+1j*_yi

            _xr, _yr, _yi = l.strip().split('\t')
            _xr, _yr, _yi = map(float, [_xr, _yr, _yi])
            xv.append(_xr)
            yv.append(_yr+1j*_yi)
    return xv, yv

def make_func_from_s(xv, yv):
    minx, maxx = min(xv), max(xv)
    f_int = interp1d(xv, yv)
    def func(_x):
        if _x < minx:
            print 'W: %.2f' %(_x-minx,)
            return f_int(minx)
        elif _x > maxx:
            print 'W: %.2f' %(_x-maxx,)
            return f_int(maxx)
        else:
            return f_int(_x)
        #if minx <= _x <= maxx:
        #     for i, tmpx in enumerate(xv):
        #         if _x <= tmpx:
        #             #return yv[i]
        #             # _X e' contenuta in i-1, i
        #             # retta passante per 2 punti
        #             x1, x2 = xv[i-1], tmpx
        #             y1, y2 = yv[i-1], yv[i]
        #             return (_x - x1)*(y2-y1)/(x2-x1) + y1
        #else:
        #    raise ValueError('mi mancano i valori per %f' %_x)
    return func

def make_func_s(f, xa, xb, n=1000):
    xv = numpy.linspace(xa, xb, n)
    fu = numpy.frompyfunc(f, 1, 1)
    yv = fu(xv)
    return xv, yv

def remove_nan(xs, ys):
    #new_xs = xs.copy()
    #new_ys = ys.copy()
    nans_i = []
    assert len(xs) == len(ys)
    for i, _x, _y in zip(range(len(xs)+1), xs, ys):
        if numpy.isnan(_y):
            print 'nan',i, _x, _y
            nans_i.append(i)
            #new_xs = numpy.delete(new_xs, i)
            #new_ys = numpy.delete(new_ys, i)
    #return new_xs, new_ys
    return numpy.delete(xs, nans_i), numpy.delete(ys, nans_i)

def write_file(fname, xv, yv):
    with open(fname, 'w') as fh:
        for x, y in zip(xv, yv):
            fh.write('%f\t%f\t%f\n' %(scipy.real(x), scipy.real(y), scipy.imag(y)))

parser = optparse.OptionParser()
parser.add_option('-L', type=float, default=1.0, help='lunghezza impulso mod in T')
parser.add_option('-T', type=float, default=0.5, help='tempo di simbolo')
parser.add_option('-H', type=float, default=0.5, help='indice di modulazione')
parser.add_option('-M', type=float, default=2.0, help='numero di simboli')
parser.add_option('-o', '--output', type=str, default='output', help='nome del grafico da generare')
parser.add_option('-p', '--plot', type=str, default='notset', help='grafico da generare (cpm, cpfsk, i, psi)')
options, args = parser.parse_args()
L=options.L
T=options.T
h=options.H

M=options.M

def getsymbols():
    allsym = range(-(int(M)-1), int(M))
    return [s for s in allsym if (s%2)==1]


P_eq = dict([(s, 1.0/M) for s in getsymbols()])

def rand_dist(N, step):
    _P = P_eq.copy()
    for i in range(N):
        _indsub = random.choice(_P.keys())
        _P[_indsub] -= step
        _indadd = random.choice(_P.keys())
        _P[_indadd] += step
    return _P
P_rand = rand_dist(100, 0.01)

P = P_eq

def g_REC(t):
    if 0<=t and t<=(L*T):
        return 1/(2*T)
    else:
        return 0

def g_RC(t):
    if 0<=t and t<=(L*T):
        return (1/(2*L*T))*(1 - scipy.cos(((2*pi*t)/(L*T))))
    else:
        return 0

g = g_REC
def q(t):
    if t<0:
        return 0
    elif 0<=t and t<=(L*T):
        return cplx_integral(g, -scipy.integrate.Inf, t)
        #return cplx_integral(g, -3000000000000, t)
    else:
        return 0.5

class Plot(object):
    fname_template = '%%s_T%.2f_M%d_L%d_h%.2f' %(T, M, L, h)
    def save_file(self, xs, ys, name):
        assert len(xs) == len(ys)
        fname = self.fname_template %(name,)
        with open(fname, 'w') as fh:
            for x, y in zip(xs, ys):
                fh.write('%s\t%s\n' %(repr(x), repr(y)))

    def read_file(self, name):
        fname = self.fname_template %name
        if os.path.exists(fname):
            with open(fname) as fh:
                lines = fh.readlines()
            ns = len(lines)
            x, y = numpy.ndarray(ns), numpy.ndarray(ns, dtype=complex)
            for i, l in enumerate(lines):
                xr, yr = l.split('\t')
                print xr
                print yr
                x[i] = eval(xr)
                y[i] = eval(yr)
            return x, y
        else:
            return None

#############################################################
# CPM

# psi = sum([P[n]*exp(1j*pi*h*n) for n in getsymbols()])

# def R_innersum(k, t, m, xi):
#     print '-'
#     return sum([P[n]*math.exp(1j*2*math.pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])
# def R_intrr(t, m, xi):
#     print 'l'
#     #return reduce(lambda tmp1, tmp2: tmp1*tmp2, [innersum(k, t)  for k in range(1-L, m+2)], 1)
#     return scipy.prod([R_innersum(k, t, m, xi)  for k in range(1-L, m+2)])

# def R(tau):
#     ##
#     m = int(tau/T)
#     xi = tau-m*T
#     def innersum(k, t):
#        return sum([P[n]*exp(1j*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])
#     def intrr(t):
#        #return reduce(lambda tmp1, tmp2: tmp1*tmp2, [innersum(k, t)  for k in range(1-L, m+2)], 1)
#        return scipy.prod([innersum(k, t)  for k in range(1-L, m+2)])
#     ##
#     def intpp(t):
#         # sembra piu veloce di intrr
#         return (1/2)*sum([P[n]*exp(1j*2*pi*h*sum([n*(q(t+tau-k*T)-q(t-k*T)) for k in range(-(L+2), L+3)]))  for n in getsymbols()])
#     ##
#     return (1/(2*T))*cplx_integral(intrr, 0, T)
#     #return (1/(T))*cplx_integral(intpp, 0, T)
#     #return (1/(2*T))*cplx_integral(lambda t: R_intrr(t, m, xi), 0, T)

# def S_CPM(f):
#     def intff(tau):
#         return R(tau)*exp(-1j*2*pi*f*tau)
#     # fy = 2*scipy.real(cplx_integral(intff, 0, L*T)+
#     #                   cplx_integral(intff, L*T, (L+1)*T)/(1-psi(1j*h)*math.exp(-1j*2*math.pi*f*T)))
#     fy = 2*scipy.real(cplx_integral(intff, 0, L*T)+
#                       cplx_integral(intff, L*T, (L+1)*T)/(1-psi*exp(-1j*2*pi*f*T)))
#     return fy

# def S_CPM_v(v):
#     ret = []
#     for f in v:
#         print '-'
#         fy = S_CPM(f)
#         ret.append(fy)
#     return ret

# # ## alternativa, vado di fft

# # def single_exp(t, tau, k):
# #     def _exp(n):
# #         arg = 1j*2*pi*h*n*(q(t+tau-k*T) - q(t-k*T))
# #         return scipy.exp(arg)
# #     return sum([P[n]*_exp(n) for n in getsymbols()])

# # def autocorr(t, tau):
# #     # approssimo sommatoria infinita siccome q(t) e' 0 fuori da 0-LT
# #     prod_elements = [single_exp(t, tau, k) for k in range(-(L+2), L+3)]
# #     return 0.5*numpy.prod(prod_elements)

# def autocorr(t, tau):
#     m = int(tau/T)
#     xi = float(tau)%T
#     def innersum(k, t):
#        return sum([P[n]*exp(1j*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])
#     return scipy.prod([innersum(k, t)  for k in range(1-L, m+2)])

# def avg_autocorr(tau):
#     # return cplx_integral(lambda t: autocorr(t, tau), 0, T)
#     xvr, yvr = make_func_s(lambda t: autocorr(t, tau), 0, T, 1000)
#     return scipy.integrate.simps(xvr, yvr)/(2*T)

# def S_CPM_fft_v(v):
#     _pts = numpy.linspace(-10*L**T, 10*L*T, len(v))
#     points = numpy.array(_pts, dtype=complex)

#     avg_ac_v = map(avg_autocorr, points)
#     #avg_ac_v = avg_autocorr(points)
#     avg_ac_fft = numpy.fft.fft(avg_ac_v)
#     return avg_ac_fft

# ### ALTERNATIVA: approssimo numericamente R e poi calcolo lo apettro con Simpson
# # approssimio R con N punti per ogni integrale
# N=1000

# psi = sum([P[n]*exp(1j*pi*h*n) for n in getsymbols()])

# def R(tau):
#     print '-'
#     m = int(tau/T)
#     xi = tau%T
#     def innersum(k, t):
#        return sum([P[n]*exp(1j*2*pi*h*n*(q(t+xi-(k-m)*T)-q(t-k*T))) for n in getsymbols()])
#     def intrr(t):
#        return scipy.prod([innersum(k, t)  for k in range(1-L, m+2)])
#     #return (1/(2*T))*cplx_integral(intrr, 0, T)
#     xvr, yvr = make_func_s(intrr, 0, T, N)
#     #print xvr, yvr
#     return scipy.integrate.simps(xvr, yvr)/(2*T)

# def init_app():
#     global xv1, yv1, xv2, yv2
#     print 'init 0'
#     xv1, yv1 = make_func_s(R, 0, L*T, N*10)
#     print xv1, yv1
#     print 'init 1'
#     xv2, yv2 = make_func_s(R, L*T, (L+1)*T, N)
#     print xv2, yv2
#     print 'init 2'

# def S_CPM_app(f):
#     def intff(args):
#         tau, Rtau = args
#         return Rtau*exp(-1j*2*pi*f*tau)
#     #print  map(intff, zip(xv1, yv1))
#     yv1f = map(intff, zip(xv1, yv1))
#     yv2f = map(intff, zip(xv2, yv2))
#     int1 = scipy.integrate.simps(yv1f, xv1)
#     int2 = scipy.integrate.simps(yv2f, xv2)
#     print int1, int2
#     fy = 2*scipy.real( + int2/(1-psi*exp(-1j*2*pi*f*T)))
#     return fy

# def S_CPM_app_v(v):
#     return map(S_CPM_app, v)

#########################
# simboli equiprobabili
# def R(tau):
#     print '.',
#     sys.stdout.flush()
#     #if tau==0:
#         # singolarita'
#     #    return 0.5

#     def innerprod(t):
#         def prod_el(t, k):
#             return scipy.sin(2*pi*h*M*(q(t+tau-k*T)-q(t-k*T)))/\
#                     (M*scipy.sin(2*pi*h*(q(t+tau-k*T)-q(t-k*T))))
#         return scipy.prod([prod_el(t, k) for k in range(1-int(L), abs(int(tau/T))+1)])
#     #return cplx_integral(innerprod, 0, T)/(2*T)
#     xvr, yvr = make_func_s(innerprod, 0, T, 100)
#     val = scipy.integrate.simps(yvr, xvr)/(2*T)
#     if numpy.isnan(val):
#         print tau, yvr
#     return val

# # psi = scipy.sin(M*pi*h)/(M*scipy.sin(pi*h))

# # print '-'
# # rxs, rys = make_func_s(R, 0, (L+1)*T, 200)
# # rxs, rys = remove_nan(rxs, rys)
# # print rxs, rys
# # Rs = make_func_from_s(rxs, rys)

# #rxs, rys = make_func_s(R, -10*T, 18*T, 100)
# #Rs = make_func_from_s(rxs, rys)
# #print rxs, rys
# #_pts = numpy.linspace(-10*L**T, 10*L*T, len(v))
# #points = numpy.array(_pts, dtype=complex)

#     #avg_ac_v = map(avg_autocorr, points)
#     #avg_ac_v = avg_autocorr(points)
# #avg_ac_fft = numpy.fft.fft(rys)
# #print '--'

# # if 1:
# #     fig = pyplot.figure()
# #     #p = fig.add_subplot(1, 1, 1, xlim=xlim, ylim=ylim, autoscale_on=False)
# #     p = fig.add_subplot(1, 1, 1)
# #     #noopt_line = p.plot(loads, busy_noopt, 'r')
# #     #opt_line = p.plot(rxs, avg_ac_fft, 'b')
# #     opt_line = p.plot(rxs, rys, 'b')
# #     pyplot.xlabel('$fT$')
# #     pyplot.ylabel('$Densit\`a \, Spettrale$')
# #     #pyplot.legend((noopt_line, opt_line), ('CSMA-CA', 'CSMA-CA ott'))
# #     fig.savefig('%s.eps' %options.output)
# # sys.exit()

# def S_CPM_eq(f):
#     print f
#     def intff(tau):
#         return Rs(tau)*scipy.cos(2*pi*f*tau)
#     r1xs, r1ys = make_func_s(intff, 0, L*T, 100)
#     r2xs, r2ys = make_func_s(intff, L*T, (L+1)*T, 100)
#     int1 = scipy.integrate.simps(r1ys, r1xs)
#     if numpy.isnan(int1):
#         print r1ys
#     int2 = scipy.integrate.simps(r2ys, r2xs)
#     if numpy.isnan(int2):
#         print r2ys
#     fy = 2*(int1+int2*(
#             (1-psi*scipy.cos(2*pi*f*T))/
#             (1+psi*psi-2*psi*scipy.cos(2*pi*f*T))
#             ))

#     # fy = 2*(cplx_integral(intff, 0, L*T)+
#     #        cplx_integral(intff, L*T, (L+1)*T)*(
#     #                         (1-psi*scipy.cos(2*pi*f*T))/
#     #                         (1+psi*psi-2*psi*scipy.cos(2*pi*f*T))
#     #                         ))
#     return fy


# --------------------------------

class CPM_eq(Plot):
    def __init__(self):
        self.psi = scipy.sin(M*pi*h)/(M*scipy.sin(pi*h))
        print '-'
        tmp = self.read_file('R')
        if tmp:
            rxs, rys = tmp
        else:
            rxs, rys = make_func_s(self.R, 0, (L+1)*T, 200)
            rxs, rys = remove_nan(rxs, rys)
            self.save_file(rxs, rys, 'R')
        self.Rs = make_func_from_s(rxs, rys)
        print '--'

    def prod_el(self, t, k, tau):
        #val = scipy.sin(2*pi*h*M*(q(t+tau-k*T)-q(t-k*T)))/\
        #    (M*scipy.sin(2*pi*h*(q(t+tau-k*T)-q(t-k*T))))
        #if numpy.isnan(val):
        #    # limite per tau->0
        #    # sfrutto sen(2x)=2sen(x)cos(x) e il fatto che M e' una potenza di 2
        #    val = scipy.prod([scipy.cos(2*pi*h*(2**(i-1))*(q(t+tau-k*T)-q(t-k*T)))
        #               for i in range(1, int(scipy.log2(M)+1))])

        val = sum([2*scipy.cos(2*pi*h*n*(q(t+tau-k*T)-q(t-k*T)))  for n in [tmp for tmp in getsymbols() if tmp>0]])

        if numpy.isnan(val):
            print 'R prod nan:',t, k, tau
        return val
    def innerprod(self, t, tau):
        return scipy.prod([self.prod_el(t, k, tau) for k in range(1-int(L), int(tau/T)+2)])
    def R(self, tau):
        print '.',
        sys.stdout.flush()
        #if tau==0:
            #return 0.5
        #if tau==1.0:
        # singolarita'
            #return 0.5
            #tau += 0.001
        #return cplx_integral(innerprod, 0, T)/(2*T)
        xvr, yvr = make_func_s(lambda t: self.innerprod(t, tau), 0, T, 200)
        val = scipy.integrate.simps(yvr, xvr)/(2*T)
        if numpy.isnan(val):
            print tau, yvr
        return val

    def intff(self, tau, f):
        return self.Rs(tau)*scipy.cos(2*pi*f*tau)
    def S(self, f):
        print f
        r1xs, r1ys = make_func_s(lambda tau: self.intff(tau, f), 0, L*T, 200)
        r2xs, r2ys = make_func_s(lambda tau: self.intff(tau, f), L*T, (L+1)*T, 200)
        int1 = scipy.integrate.simps(r1ys, r1xs)
        if numpy.isnan(int1):
            print r1ys
        int2 = scipy.integrate.simps(r2ys, r2xs)
        if numpy.isnan(int2):
            print r2ys
        fy = 2*(int1+int2*(
                (1-self.psi*scipy.cos(2*pi*f*T))/
                (1+self.psi**2-2*self.psi*scipy.cos(2*pi*f*T))
                ))

        # fy = 2*(cplx_integral(intff, 0, L*T)+
        #        cplx_integral(intff, L*T, (L+1)*T)*(
        #                         (1-psi*scipy.cos(2*pi*f*T))/
        #                         (1+psi*psi-2*psi*scipy.cos(2*pi*f*T))
        #                         ))
        return fy

#rxs, rys = make_func_s(R, -10*T, 18*T, 100)
#Rs = make_func_from_s(rxs, rys)
#print rxs, rys
#_pts = numpy.linspace(-10*L**T, 10*L*T, len(v))
#points = numpy.array(_pts, dtype=complex)

    #avg_ac_v = map(avg_autocorr, points)
    #avg_ac_v = avg_autocorr(points)
#avg_ac_fft = numpy.fft.fft(rys)

# if 1:
#     fig = pyplot.figure()
#     #p = fig.add_subplot(1, 1, 1, xlim=xlim, ylim=ylim, autoscale_on=False)
#     p = fig.add_subplot(1, 1, 1)
#     #noopt_line = p.plot(loads, busy_noopt, 'r')
#     #opt_line = p.plot(rxs, avg_ac_fft, 'b')
#     opt_line = p.plot(rxs, rys, 'b')
#     pyplot.xlabel('$fT$')
#     pyplot.ylabel('$Densit\`a \, Spettrale$')
#     #pyplot.legend((noopt_line, opt_line), ('CSMA-CA', 'CSMA-CA ott'))
#     fig.savefig('%s.eps' %options.output)
# sys.exit()



#############################################################
# CPFSK

phi = math.sin(M*math.pi*h)/(M*math.sin(math.pi*h))

def alfa(n, m):
    return math.pi*h*(m+n-1-M)

def A(n, f):
    arg = math.pi*(f*T-0.5*h*(2*n -1 -M))
    return (math.sin(arg))/arg

def B(n, m, f):
    num = math.cos(2*math.pi*f*T - alfa(n, m)) - phi*math.cos(alfa(n, m))
    den = 1 + phi**2 -2*phi*math.cos(2*math.pi*f*T)
    return num/den

def S_CPFSK(f):
    sum1 = sum([A(n, f)**2 for n in range(1, M+1)])
    sum2 = sum([sum([B(n, m, f)*A(n, f)*A(m, f) for m in range(1, M+1)]) for n in range(1, M+1)])
    fy = T*((1/M)*sum1 + (2/(M**2))*sum2)
    return fy

def S_CPFSK_v(v):
    ret = []
    for f in v:
        fy = S_CPFSK(f)
        ret.append(fy)
    return ret

#############################################################

if options.plot.startswith('c'):
    n=max(M/2.0, 2.0)
    minf=0 #-n/T
    maxf=n/T  # fT=n
    print 'grafico da %.2f a %.2f' %(minf, maxf)

    fv = numpy.linspace(minf, maxf, 200)
    #sf = [abs(S(ff)) for ff in f]
    if options.plot == 'cpm':
        #init_app()
        #sf = S_CPM_app_v(fv)
        #sf = S_CPM_fft_v(fv)
        #sf = map(S_CPM_eq, fv)
        sf = map(CPM_eq().S, fv)
        Plot().save_file(fv, sf, 'S')
        #sf = map(R, fv)
        #make_file_func(R, 0, (L+1)*T)
        #sys.exit()
        #_area = cplx_integral(S_CPM, -scipy.integrate.Inf, scipy.integrate.Inf)
        #_area = cplx_integral(S_CPM, minf, maxf)
        #_area = cplx_integral(R, minf, maxf)
        _area = 0

    elif options.plot == 'cpfsk':
        sf = S_CPFSK_v(fv)
        # PERCHE' bisogna normalizzare con 1/2T ???
        sf = map(lambda f: (1.0/(2.0*T))*abs(f), sf)
        #_area = cplx_integral(S_CPFSK, -scipy.integrate.Inf, scipy.integrate.Inf)
        #_area = cplx_integral(S_CPFSK, minf, maxf)
        _area = 0
    else:
        print 'Error: plot:', options.plot
        sys.exit(1)

    #print 'f(0):',S_CPM(0)
    print 'integrale -inf +inf:', _area

    x = fv*T
    #y = map(lambda f: (1.0/(2.0*T))*abs(f), sf)
    y = sf

    # print min(x), max(x)
    # print cplx_integral(tmp, -scipy.integrate.Inf, scipy.integrate.Inf)
    # y = map(tmp, x)
    print y
    fig = pyplot.figure()
    p = fig.add_subplot(1, 1, 1, xlim=(min(x), max(x)), ylim=(0, 1), autoscale_on=False)
    #p = fig.add_subplot(1, 1, 1)
    #noopt_line = p.plot(loads, busy_noopt, 'r')
    opt_line = p.plot(x, y, 'b')
    pyplot.xlabel('$fT$')
    pyplot.ylabel('$Densit\`a \, Spettrale$')
    #pyplot.legend((noopt_line, opt_line), ('CSMA-CA', 'CSMA-CA ott'))
    fig.savefig('%s.eps' %options.output)

elif options.plot == 'i':
    minx = -T
    maxx = (L+1)*T
    x = numpy.linspace(minx, maxx, 500)
    #y = [g_REC(t) for t in x]
    #y = [q(t, g_REC) for t in x]
    #y = [g_RC(t) for t in x]
    y = [q(t, g_RC) for t in x]
    xlim = [minx, maxx]
    ymax = max(scipy.real(y))
    ylim = [0, ymax*1.5]
    #ymax_RC = max(scipy.real(y3))
    #ylim_RC = [-0.05, ymax_RC*1.5]

    fig = pyplot.figure(1)

    # grafico g REC
    ax = SubplotZero(fig, 111, ylim=ylim, xlim=xlim, autoscale_on=False)
    fig.add_subplot(ax)

    opt_line = ax.plot(x, y, 'b')#, x1, y1, 'k--')

    ax.axis['xzero'].set_visible(True)
    ax.axis['yzero'].set_visible(True)
    formatter = ticker.FormatStrFormatter('%1.1fT ')
    ax.xaxis.set_major_formatter(formatter)
    ax.set_yticks([ymax, ylim[1]-0.0001])
    ax.set_yticklabels(['$\\frac{1}{2}$', '$q(t)$'])
    ax.set_xlabel('(d)')
    for direction in ["left", "right", "bottom", "top"]:
        ax.axis[direction].set_visible(False)

    fig.savefig('%s.eps' %options.output)

elif options.plot == 'psi':
    def psi(h):
         return sum([P[n]*scipy.exp(1j*scipy.pi*h*n) for n in getsymbols()])

    x = numpy.linspace(0, 5, 10)
    y = map(psi, x)
    fig = pyplot.figure()
    p = fig.add_subplot(1, 1, 1, xlim=[0, 5], ylim=[-1.1, 1.1], autoscale_on=False)
    opt_line = p.plot(x, y, 'b')
    fig.savefig('%s.eps' %options.output)

elif options.plot == 'r':
    # calcolo R per CPM
    x = numpy.linspace(0, (L+1)*T, 10**6)
    y = map(R, x)

else:
    print 'Errore: plot =',options.plot
    #sys.exit(1)

#x = points
#y = avg_ac_fft

#print x
#print y

#fig = pyplot.figure()
#p = fig.add_subplot(1, 1, 1, xlim=xlim, ylim=ylim, autoscale_on=False)
##noopt_line = p.plot(loads, busy_noopt, 'r')
#opt_line = p.plot(x, y, 'b')
##pyplot.legend((noopt_line, opt_line), ('CSMA-CA', 'CSMA-CA ott'))
#fig.savefig('%s.svg' %options.output)
