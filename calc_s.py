#!/usr/bin/env python

import os
import sys
import itertools

from scipy import pi, exp
import scipy
import numpy

from common import *
from calc_cpfsk import DensCPFSK

class DensEq(FileFunc):
    def __init__(self):
        res = self.read_file('R')
        res1 = self.read_file('R1')
        if res and res1:
            self.rxy = res
            self.rxy1 = res1
            #xr, yr = res
            #xr1, yr1 = res1
            #self.Rs = make_func_from_s(xr, yr)
            #self.Rs1 = make_func_from_s(xr1, yr1)
        else:
            raise ValueError('mi manca il file di R')
        self.psi = scipy.sin(M*pi*h)/(M*scipy.sin(pi*h))

    #def intff(self, tau, f):
        #return self.Rs(tau)*scipy.cos(2*pi*f*tau)
    def intff(self, Rstau, tau, f):
        return Rstau*scipy.cos(2*pi*f*tau)
    intff_v = numpy.frompyfunc(intff, 4, 1)
    def S(self, f):
        print '-',
        sys.stdout.flush()
        #r1xs, r1ys = make_func_s(lambda tau: self.intff(tau, f), 0, L*T, 200)
        #r2xs, r2ys = make_func_s(lambda tau: self.intff(tau, f), L*T, (L+1)*T, 200)
        #r1xs, r1ys = self.rxy[0], map(lambda args: self.intff(args[0], args[1], f), [(self.rxy[1][i], self.rxy[0][i]) for i in range(len(self.rxy[0]))])#self.intff_v(self, self.rxy[1], self.rxy[0], f)
        #r2xs, r2ys = self.rxy1[0], self.intff_v(self, self.rxy1[1], self.rxy1[0], f)
        #r2xs, r2ys = self.rxy[0], map(lambda args: self.intff(args[0], args[1], f), [(self.rxy1[1][i], self.rxy1[0][i]) for i in range(len(self.rxy1[0]))])#self.intff_v(self, self.rxy[1], self.rxy[0], f)

        r1xs, r1ys = self.rxy[0], self.rxy[1]*scipy.cos(2*pi*f*self.rxy[0])
        r2xs, r2ys = self.rxy1[0], self.rxy1[1]*scipy.cos(2*pi*f*self.rxy1[0])

        int1 = scipy.integrate.simps(r1ys, r1xs)
        if numpy.isnan(int1):
            print r1ys
        int2 = scipy.integrate.simps(r2ys, r2xs)
        if numpy.isnan(int2):
            print r2ys
        fy = 2*(int1+int2*(
                (1-self.psi*scipy.cos(2*pi*f*T))/
                (1+self.psi**2-2*self.psi*scipy.cos(2*pi*f*T))
                ))

        #fy = 2*scipy.real(int1+int2*(1-self.psi*exp(1j*2*pi*f*T)))

        # fy = 2*(cplx_integral(intff, 0, L*T)+
        #        cplx_integral(intff, L*T, (L+1)*T)*(
        #                         (1-psi*scipy.cos(2*pi*f*T))/
        #                         (1+psi*psi-2*psi*scipy.cos(2*pi*f*T))
        #                         ))
        return fy

class DensCPM(FileFunc):
    def __init__(self):
        res = self.read_file('R')
        res1 = self.read_file('R1')
        if res and res1:
            self.rxy = res
            self.rxy1 = res1
            #xr, yr = res
            #xr1, yr1 = res1
            #self.Rs = make_func_from_s(xr, yr)
            #self.Rs1 = make_func_from_s(xr1, yr1)
        else:
            raise ValueError('mi manca il file di R')
        self.psi =  sum([P[n]*exp(1j*pi*h*n) for n in getsymbols()])

    def cplx_simps(self, x, y):
        rint = scipy.integrate.simps(scipy.real(y), x)
        iint = scipy.integrate.simps(scipy.imag(y), x)
        return complex(rint, iint)

    def S(self, f):
        print '-',
        sys.stdout.flush()
        r1xs, r1ys = self.rxy[0], self.rxy[1]*exp(-1j*2*pi*f*self.rxy[0])
        r2xs, r2ys = self.rxy1[0], self.rxy1[1]*exp(-1j*2*pi*f*self.rxy1[0])

        int1 = scipy.integrate.simps(r1ys, r1xs)
        #int1 = self.cplx_simps(r1xs, r1ys)
        if numpy.isnan(int1):
            print r1ys

        int2 = scipy.integrate.simps(r2ys, r2xs)
        #int2 = self.cplx_simps(r2xs, r2ys)
        if numpy.isnan(int2):
            print r2ys

        int2p = 1/(1-self.psi*exp(-1j*2*pi*f*T))
        fy = 2*scipy.real(int1 + int2p*int2)

        return fy

# class DensBoh(FileFunc):
#     def __init__(self):
#         self.psi = scipy.sin(M*pi*h)/(M*scipy.sin(pi*h))
#     def init_r(self, m):
#         self.K = int(m+L+1)
#         self.N = int(M/2)
#         self.n_sets = self.N**self.K
#         self.n_comb = 2**(self.K-1)
#         self._kk = range(int(1-L), int(m+2))
#         self._nn = [s for s in range(int(M)) if (s%2)==1]
#         print self._nn

#         # genero K insiemi di N elementi
#         #pre_sets = [[(n, k) for n in self._nn] for k in self._kk]
#         #pre_indexes = [[(n, k) for n in range(self.N)] for k in range(self.K)]
#         # genero N^K insiemi di K elementi prendendo un elemento da ogni pre_sets
#         elems = [(k, n) for n in self._nn for k in self._kk]
#         self.symtab = []
#         for i in itertools.combinations(elems, self.K):
#             # tolgo dalle combinazioni quelle con piu
#             # di un elemento per ogni insieme
#             sets = set()
#             for ii in i[:]:
#                 if ii[0] in sets:
#                     break
#                 else:
#                     sets.add(ii[0])
#             else:
#                 self.symtab.append(i)
#         #print pre_sets, self.N, self.K, m, pre_indexes
#         # genero N^K insiemi di K elementi prendendo un elemento da ogni pre_sets
#         # self.symtab = []
#         # for _ in range(self.N**self.K):
#         #     print self.symtab
#         #     new = []
#         #     for s in pre_sets:
#         #         new.append(s.pop())
#         #     self.symtab.append(new)
#         #print self.symtab
#         # per ogni insieme di elementi, genero 2^(K-1) combinazioni
#         self.combtab = []
#         # for s in self.symtab:
#         #     n_sym = len(s)
#         #     comb_list = []
#         #     # le combinazioni possono essere generate dalla rappresentazione
#         #     # binaria del numero di combinazione
#         #     for n in range(2**(self.K-1)):
#         #         n_comb = []
#         #         n_bin = "".join(map(lambda y:str((n>>y)&1), range(n_sym-1, -1, -1)))
#         #         for c in n_bin:
#         #             if c == '0':
#         #                 sym = +1
#         #             else:
#         #                 sym = -1
#         #             n_comb.append(sym)
#         #         comb_list.append(n_comb)
#         #     self.combtab.append(comb_list)
#         for s in self.symtab:
#             comb_list = []
#             for i in itertools.product([-1, 1], repeat=self.K):
#                 if i[0] == 1:
#                     comb_list.append(i)
#                     #print i
#             self.combtab.append(comb_list)
#         #print self.combtab

#     def _S(self, l, i, j):
#         # m va da 0 a L
#         # M=2 L=2
#         # combtab = [
#         #     # m=0
#         #     [# insieme 1
#         #         [# combinazioni
#         #             [ +1, +1, +1 ],
#         #             [ +1, +1, -1 ],
#         #             [ +1, -1, +1 ],
#         #             [ +1, -1, -1 ]
#         #             ]
#         #         ],
#         #     # m=1
#         #     [#insieme 1
#         #         [#combinazioni
#         #             [ +1, +1, +1, +1 ],
#         #             [ +1, +1, +1, -1 ],
#         #             [ +1, +1, -1, +1 ],
#         #             [ +1, +1, -1, -1 ],
#         #             [ +1, -1, +1, +1 ],
#         #             [ +1, -1, +1, -1 ],
#         #             [ +1, -1, -1, +1 ],
#         #             [ +1, -1, -1, -1 ]
#         #             ]
#         #         ],
#         #     # m=2
#         #     [#insieme 1
#         #         [#combinazioni
#         #             [ +1, +1, +1, +1, +1 ],
#         #             [ +1, +1, +1, +1, -1 ],
#         #             [ +1, +1, +1, -1, +1 ],
#         #             [ +1, +1, +1, -1, -1 ],
#         #             [ +1, +1, -1, +1, +1 ],
#         #             [ +1, +1, -1, +1, -1 ],
#         #             [ +1, +1, -1, -1, +1 ],
#         #             [ +1, +1, -1, -1, -1 ],
#         #             [ +1, -1, +1, +1, +1 ],
#         #             [ +1, -1, +1, +1, -1 ],
#         #             [ +1, -1, +1, -1, +1 ],
#         #             [ +1, -1, +1, -1, -1 ],
#         #             [ +1, -1, -1, +1, +1 ],
#         #             [ +1, -1, -1, +1, -1 ],
#         #             [ +1, -1, -1, -1, +1 ],
#         #             [ +1, -1, -1, -1, -1 ]
#         #             ]
#         #         ]
#         #     ]
#         # M=2 L=1
#         # combtab = [
#         #     # m=0
#         #     [# insieme 1
#         #         [# conbinazioni
#         #             [ +1, +1 ],
#         #             [ +1, -1 ]
#         #             ]
#         #         ],
#         #     # m=1
#         #     [# insieme 1
#         #         [# combinazioni
#         #             [ +1, +1, +1 ],
#         #             [ +1, +1, -1 ],
#         #             [ +1, -1, +1 ],
#         #             [ +1, -1, -1 ]
#         #             ]
#         #         ],
#         #     ]
#         #print m, i, j, l
#         #return combtab[int(m)][i][j][l]
#         return self.combtab[i][j][l]
#     def fi(self, i, l, t, tau):
#         # M=2 L=1
#         # symtab = [
#         #     [#m=0
#         #         [# insieme 1
#         #             (1, 0),
#         #             (1, 1)
#         #             ]
#         #         ],
#         #     [#m=1
#         #         [# insieme 1
#         #             (1, 0),
#         #             (1, 1),
#         #             (1, 2)
#         #             ]
#         #         ]
#         #     ]
#         # # M=2 L=2
#         # symtab = [
#         #     [# m=0
#         #         [# insieme 1
#         #             (1, -1),
#         #             (1, 0),
#         #             (1, 1)
#         #                 ]
#         #         ],
#         #     [# m=1
#         #         [# insieme 1
#         #             (1, -1),
#         #             (1, 0),
#         #             (1, 1),
#         #             (1, 2)
#         #             ]
#         #         ],
#         #     [# m=2
#         #         [# insieme 1
#         #             (1, -1),
#         #             (1, 0),
#         #             (1, 1),
#         #             (1, 2),
#         #             (1, 3)
#         #             ]
#         #         ]
#         #     ]
#         #n, k = symtab[int(m)][i][l]
#         n, k = self.symtab[i][l]
#         return self._fi(n, k, t, tau)
#     def _fi(self, n, k, t, tau):
#         ret = 2*pi*h*n*(q(t+tau-k*T)-q(t-k*T))
#         #if k == 1 and ret != 0:
#         #    print n, k, t, tau
#         return ret
#     def a(self, i, j, t, tau):
#         ret = sum([self._S(l, i, j)*self.fi(i, l, t, tau) for l in range(0, self.K)])
#         return ret
#     def r_int(self, i, j, tau):
#         def r_intf(t):
#             ret = scipy.cos(self.a(i, j, t, tau))
#             return ret
#         ret, err = scipy.integrate.quad(r_intf, 0, T)
#         #ret = scipy.integrate.romberg(r_intf, 0, T)
#         #ret = self._simps(r_intf, 0, T)
#         return ret
#     def R(self, tau, m):
#         self.init_r(m)
#         def innersum(i):
#             return sum([ self.r_int(i, j, tau)
#                          for j in range(0, self.n_comb)])
#         extsum = sum([ innersum(i) for i in range(0, self.n_sets) ])
#         ret = (self.K/(T*2**(self.K-1)))*extsum
#         return ret

#     def _simps(self, f, a, b):
#         x = numpy.linspace(a, b, 50)
#         y = map(f, x)
#         return scipy.integrate.simps(y, x)

#     def s_int(self, a, b, f, m):
#         def f_int(tau):
#             return self.R(tau, m)*scipy.cos(2*pi*f*tau)
#         ret, err = scipy.integrate.quad(f_int, a, b)
#         #ret = scipy.integrate.romberg(f_int, a, b)
#         #ret = self._simps(f_int, a, b)
#         return ret
#     def S(self, f):
#         intsum = sum([self.s_int(m*T, (m+1)*T, f, m) for m in range(0, int(L))])
#         print '.',
#         sys.stdout.flush()
#         ret = 2*(intsum + (1-self.psi*exp(1j*2*pi*f*T))*self.s_int(L*T, (L+1)*T, f, L))
#         return ret

def calc_s(f):
    fc = FileFunc()
    rx, ry = fc.read_file('R')
    r1x, r1y = fc.read_file('R1')
    psi =  sum([P[n]*exp(1j*pi*h*n) for n in getsymbols()])

    # dimensioni: f, tau
    #             0, 1
    f2 = f.reshape((-1, 1))
    rx = rx.reshape((1, -1))
    ry = ry.reshape((1, -1))
    r1x = r1x.reshape((1, -1))
    r1y = r1y.reshape((1, -1))
    y = 2*scipy.real(scipy.integrate.simps(ry*exp(-1j*2*pi*f2*rx), rx, axis=1) +\
                         scipy.integrate.simps(r1y*exp(-1j*2*pi*f2*r1x), r1x, axis=1)/\
                         (1-psi*exp(-1j*2*pi*f*T)))
    return y

if __name__=='__main__':
    n=max(M/2.0, 2.0)
    minf=0 #-n/T
    maxf=n/T  # fT=n

    if options.modo == 'default':
        if (L == 1 and options.g == 'rec'):
            tc = DensCPFSK()
        else:
            #tc =DensEq()
            tc =DensCPM()
    elif options.modo == 'cpm':
        #tc =DensEq()
        tc =DensCPM()
    elif options.modo == 'cpfsk':
        tc = DensCPFSK()
    #tc = DensBoh()
    x = numpy.linspace(minf, maxf, 200)
    #fn = Test().R
    #fn = lambda tau: tc.innerprod(0, tau)
    #tc.prod_el = tc.prod_el1
    #fn = lambda tau: tc.innerprod(0, tau)
    fn = tc.S
    #tc.prod_el = tc.prod_el2
    #fn1 = lambda tau: tc.innerprod(0, tau)
    #fn1 = tc.R
    f = numpy.frompyfunc(fn, 1, 1)
    #f1 = numpy.frompyfunc(fn1, 1, 1)
    #y = f(x)
    y = calc_s(x)

    #y1 = f1(x)
    #for a, b in zip(y, y1):
    #    if a-b != 0:
    #        print a, b, a-b

    tc.save_file(x, y, 'S')
