#!/usr/bin/env python

import os
import sys
import scipy
import numpy
from scipy import pi, exp, cos, sin

from matplotlib import pyplot
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.ticker as ticker

import common as c
import calc_s
import calc_cpfsk

def load(name=c.options.input_file):
    F = c.FileFunc()
    x, y = F.read_file(name)
    return x, y

def genmulcosf(x, y):
    def intf(f):
        print '.',
        sys.stdout.flush()
        #for i, _x in enumerate(x):
        #    #y[i] = y[i]*cos(s*pi*f*_x)
        #    y[i] = y[i]*exp(-1j*2*pi*f*_x)
        return y*exp(-1j*2*pi*f*x)
    return numpy.frompyfunc(intf, 1, 1)

def calc_cpm(rx, ry, rx1, ry1, minf, maxf):
    print '-'
    xf = scipy.linspace(minf, maxf, 300)
    psi =  sum([c.P[n]*exp(1j*pi*c.h*n) for n in c.getsymbols()])
    yf = []
    yfunc = genmulcosf(rx, ry)
    yfunc1 = genmulcosf(rx1, ry1)
    for f in xf:
        int1 = scipy.integrate.simps(yfunc(f), rx)
        int2 = (1/(1-psi*exp(-1j*2*pi*f*c.T)))*scipy.integrate.simps(yfunc1(f), rx1)
        #yf.append(2*scipy.real(int1+int2))
        yf.append(2*scipy.real(int1+int2))
    return xf, numpy.array(yf)

def load_S(limits=None, nx=200):
    print 'Carico dati per L=%d M=%d h=%.2f g=%s' %(c.L, c.M, c.h, c.options.g)
    if limits:
        minf, maxf = limits
    else:
        minf, maxf = 0, max(c.M/2.0, 2.0)/c.T
    if c.L == 1 and c.options.g == 'rec':
        reload(calc_cpfsk)
        tc = calc_cpfsk.DensCPFSK()
        fn = tc.S
        f = numpy.frompyfunc(fn, 1, 1)
    else:
        reload(calc_s)
        #tc = calc_s.DensCPM()
        tc = calc_s.DensEq()
        fn = tc.S
        f = numpy.frompyfunc(fn, 1, 1)
        #f = calc_s.calc_s
    x = scipy.linspace(minf, maxf, nx)
    y= f(x)
    #x = x/c.T
    #y = y*c.T
    # xr, yr = c.FileFunc().read_file('R')
    # xr1, yr1 = c.FileFunc().read_file('R1')
    # x, y = calc_cpm(xr, yr, xr1, yr1, minf, maxf)
    return x, y

def load_oob(n, nx=200):
    x, y = load_S((0, n/c.T), nx=nx)
    #x, y = x*c.T, y/c.T
    x = x*c.T
    #Tb = c.T/scipy.log2(c.M)
    #x = x*Tb
    #y = y.astype('float128')
    #ref = y.item(0).repeat(nx)
    #ref = float(y.max())
    #y /= ref
    y = y.astype('float128')
    #c.write_file(x, y, 'oobpre')
    y = 10*scipy.log10(y)
    #c.write_file(x, y, 'oobdata')
    return x, y

def load_asyn(k, x0=0, y0=0, ftn=15):
    x = scipy.linspace(1, ftn/c.T)
    y = (x-x0)**(-k) + y0
    ymax = float(y.item(0))
    y /= ymax
    y = 10*scipy.log10(y)
    return x*c.T, y

def load_mod(m=2.0, h=0.5, l=1.0, g='rec', d='data'):
    a0 = sys.argv[0]
    new_argv = [a0, '-M', str(m), '-H', str(h), '-L', str(l), '-g', g, '-d', d]
    sys.argv = new_argv
    reload(c)
    # reload(calc_s)

if __name__=='__main__':
    lM = [2, 4, 8]
    lL = [1, 2, 3, 4]
    lh1 = [0.3, 0.4, 0.5, 0.6]
    lh2 = [0.7,  0.8, 0.9, 0.95]
    lh3 = [1.05, 1.2, 1.3]
    lh4 = [1.5, 1.9, 2.3]
    lhrc = [0.5, 0.8]

    # # grafico comparativo per M
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for m in lM:
    #     load_mod(m=m)
    #     #x, y = load('S')
    #     x, y = load_S((0, 8))
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.3, 0.6, 'M=2')
    # ax.text(0.85, 0.2, 'M=4')
    # ax.text(1.8, 0.1, 'M=8')
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_M.eps')

    # # grafico comparativo per M h=0.8
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for m in lM:
    #     load_mod(m=m, h=0.8)
    #     #x, y = load('S')
    #     x, y = load_S((0, 8))
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.6, 0.6, 'M=2')
    # ax.text(0.85, 0.2, 'M=4')
    # ax.text(1.8, 0.1, 'M=8')
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_M_h0.8.eps')

    # # grafico comparativo per M h=0.95
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for m in lM:
    #     load_mod(m=m, h=0.95)
    #     #x, y = load('S')
    #     x, y = load_S((0, 8))
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.6, 1, 'M=2')
    # ax.text(1.6, 0.4, 'M=4')
    # ax.text(2.7, 0.1, 'M=8')
    # ax.set_ylim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_M_h0.95.eps')

    # # grafico comparativo per M h=1.05
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for m in lM:
    #     load_mod(m=m, h=1.05)
    #     #x, y = load('S')
    #     x, y = load_S((0, 8))
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.6, 1, 'M=2')
    # ax.text(1.6, 0.4, 'M=4')
    # ax.text(2.7, 0.1, 'M=8')
    # ax.set_ylim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_M_h1.05.eps')

    # # grafico comparativo per M h=1.2
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for m in lM:
    #     load_mod(m=m, h=1.2)
    #     #x, y = load('S')
    #     x, y = load_S((0, 8))
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.7, 0.6, 'M=2')
    # ax.text(1.7, 0.25, 'M=4')
    # ax.text(3.0, 0.13, 'M=8')
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_M_h1.2.eps')

    # # grafico comparativo per L
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for l in lL:
    #     load_mod(l=l)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.4, 0.3, 'L=1')
    # ax.text(0.15, 1.0, 'L=2')
    # ax.text(0.1, 1.3, 'L=3')
    # ax.text(0.1, 1.5, 'L=4')
    # ax.set_ylim(0, 2)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_L.eps')

    # # grafico comparativo per L h=0.8
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for l in lL:
    #     load_mod(l=l, h=0.8)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.55, 0.3, 'L=1')
    # ax.text(0.05, 0.5, 'L=2')
    # #ax.text(0.25, 0.8, 'L=3')
    # ax.annotate('L=3', (0.095, 0.795), xytext=(40, 17), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # ax.text(0.05, 1, 'L=4')
    # ax.set_ylim(0, 2)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_L_h0.8.eps')

    # # grafico comparativo per L h=0.95
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for l in lL:
    #     load_mod(l=l, h=0.95)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.2, 0.15, 'L=1')
    # ax.text(0.05, 0.45, 'L=2')
    # #ax.text(0.1, 1.3, 'L=3')
    # ax.annotate('L=3', (0.1, 0.67), xytext=(40, 17), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # ax.text(0.1, 0.8, 'L=4')
    # ax.set_ylim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_L_h0.95.eps')

    # # grafico comparativo per L h=1.05
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for l in lL:
    #     load_mod(l=l, h=1.05)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.2, 0.18, 'L=1')
    # ax.text(0.05, 0.43, 'L=2')
    # #ax.text(0.1, 1.3, 'L=3')
    # ax.annotate('L=3', (0.1, 0.6), xytext=(40, 17), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # ax.text(0.1, 0.72, 'L=4')
    # ax.set_ylim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_L_h1.05.eps')

    # # grafico comparativo per L h=1.2
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for l in lL:
    #     load_mod(l=l, h=1.2)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.6, 1.3, 'L=1')
    # ax.text(0.15, 0.25, 'L=2')
    # #ax.text(0.3, 0.51, 'L=3')
    # ax.annotate('L=3', (0.28, 0.48), xytext=(10, 20), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # ax.text(0.05, 0.68, 'L=4')
    # ax.set_ylim(0, 2)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_L_h1.2.eps')

    # # grafico comparativo per h piccoli
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for h in lh1:
    #     load_mod(h=h)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.08, 1.6, 'h=0.3')
    # ax.text(0.16, 0.8, 'h=0.4')
    # #ax.text(0.17, 0.23, 'h=0.5')
    # ax.annotate('h=0.5', (0.2, 0.6), xytext=(50, 30), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # ax.text(0.5, 0.25, 'h=0.6')
    # ax.set_ylim(0, 2.5)
    # ax.set_xlim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_h_small.eps')

    # # grafico comparativo per h medi bassi
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for h in lh2:
    #     load_mod(h=h)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.1, 0.45, 'h=0.7')
    # ax.text(0.3, 0.75, 'h=0.8')
    # ax.text(0.35, 1.5, 'h=0.9')
    # ax.annotate('h=0.95', (0.43, 0.24), xytext=(50, 0), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # #ax.text(0.3, 0.08, 'h=0.95')
    # ax.set_ylim(0, 2.5)
    # ax.set_xlim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_h_medium_low.eps')

    # # grafico comparativo per h medi alti
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for h in lh3:
    #     load_mod(h=h)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.55, 2, 'h=1.05')
    # ax.text(0.57, 0.95, 'h=1.2')
    # ax.text(0.68, 0.5, 'h=1.3')
    # #ax.annotate('h=1', (0.6, 0.2), xytext=(50, 30), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    # ax.set_ylim(0, 2.5)
    # ax.set_xlim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_h_medium_high.eps')

    # # grafico comparativo per h grandi
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # for h in lh4:
    #     load_mod(h=h)
    #     x, y = load('S')
    #     x, y = x*c.T, y/c.T
    #     ax.plot(x, y, 'k')
    # ax.text(0.5, 0.55, 'h=1.5')
    # ax.text(1.05, 1.3, 'h=1.9')
    # ax.text(1.2, 0.55, 'h=2.3')
    # ax.set_ylim(0, 2)
    # #ax.set_xlim(0, 1.5)
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale$')
    # fig.savefig('cpm_h_big.eps')

    # grafico comparativo per h piccoli rc/rec
    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1)
    for l in [1, 2]:
        load_mod(l=l)
        x, y = load('S')
        x, y = x*c.T, y/c.T
        rec_line = ax.plot(x, y, 'k--')
    for l in 1, 2:
        load_mod(l=l, g='rc')
        x, y = load('S')
        x, y = x*c.T, y/c.T
        rc_line = ax.plot(x, y, 'k')
    ax.annotate('L=2', (0.095, 0.85), xytext=(50, 50), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    ax.annotate('', (0.08, 1.092), xytext=(47, 0), textcoords='offset points', arrowprops=dict(arrowstyle='->', connectionstyle='arc'))
    ax.annotate('L=1', (0.04, 0.68), xytext=(0, -40), textcoords='offset points', arrowprops=dict(arrowstyle='->'))
    ax.annotate('', (0.14, 0.68), xytext=(-20, -30), textcoords='offset points', arrowprops=dict(arrowstyle='->', connectionstyle='arc'))
    ax.set_ylim(0, 1.5)
    ax.set_xlim(0, 1.2)
    pyplot.legend((rec_line, rc_line), ('LREC', 'LRC'))
    pyplot.xlabel('$fT$')
    pyplot.ylabel('$Densit\`a \, Spettrale$')
    fig.savefig('cpm_rcrec_L.eps')

    # grafico comparativo rec/rc L=1
    ftn=17
    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1)
    load_mod(l=1, d='databig')
    x, y = load_oob(ftn, 600)
    ax.plot(x, y, 'k')
    load_mod(l=1, g='rc', d='databig')
    x, y = load_oob(ftn, 600)
    ax.plot(x, y, 'k')
    x, y = load_asyn(4, ftn=ftn)
    #line4 = ax.plot(x, y-10, 'k--')
    line4 = ax.plot(x, y, 'k--')
    x, y = load_asyn(8, ftn=ftn)
    line8 = ax.plot(x, y, 'k-.')
    pyplot.legend((line4, line8), ('$|f|^{-4}$', '$|f|^{-8}$'))
    ax.text(8, -58, '1REC')
    ax.text(6, -130, '1RC')
    pyplot.xlabel('$fT$')
    pyplot.ylabel('$Densit\`a \, Spettrale \, (dB)$')
    fig.savefig('cpm_rcrec_L1.eps')

    # # grafico comparativo rec/rc L=2
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # # for l in [1, 2, 3, 4]:
    # load_mod(l=2, d='databig')
    # x, y = load_oob(15)
    # ax.plot(x, y, 'k')
    # load_mod(l=2, g='rc', d='databig')
    # x, y = load_oob(15)
    # ax.plot(x, y, 'k')
    # x, y = load_asyn(4)
    # ax.plot(x, y)
    # x, y = load_asyn(8)
    # ax.plot(x, y)
    # ax.text(8, -57, '2REC')
    # ax.text(4.5, -100, '2RC')
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale \, (dB)$')
    # fig.savefig('cpm_rcrec_L2.eps')

    # # grafico comparativo rec/rc L=3
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # load_mod(l=3, d='databig')
    # x, y = load_oob(15, 600)
    # ax.plot(x, y, 'k')
    # load_mod(l=3, g='rc', d='databig')
    # x, y = load_oob(15, 600)
    # ax.plot(x, y, 'k')
    # x, y = load_asyn(4)
    # ax.plot(x, y)
    # x, y = load_asyn(8)
    # ax.plot(x, y)
    # ax.text(8, -60, '3REC')
    # ax.text(8.2, -88, '3RC')
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale \, (dB)$')
    # fig.savefig('cpm_rcrec_L3.eps')

    # # grafico comparativo rec/rc L=4
    # fig = pyplot.figure()
    # ax = fig.add_subplot(1, 1, 1)
    # load_mod(l=4, d='databig')
    # x, y = load_oob(15, 600)
    # ax.plot(x, y, 'k')
    # load_mod(l=4, g='rc', d='databig')
    # x, y = load_oob(15, 600)
    # ax.plot(x, y, 'k')
    # x, y = load_asyn(4)
    # ax.plot(x, y)
    # x, y = load_asyn(8)
    # ax.plot(x, y)
    # ax.text(8, -60, '4REC')
    # ax.text(6.5, -90, '4RC')
    # pyplot.xlabel('$fT$')
    # pyplot.ylabel('$Densit\`a \, Spettrale \, (dB)$')
    # fig.savefig('cpm_rcrec_L4.eps')
