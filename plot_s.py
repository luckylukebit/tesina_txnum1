#!/usr/bin/env python

import sys
import optparse
from matplotlib import pyplot
from mpl_toolkits.axes_grid.axislines import SubplotZero
import matplotlib.ticker as ticker

from common import *

if __name__=='__main__':
    F = FileFunc()
    res = F.read_file(options.input_file)
    if res:
        x, y = res
        import scipy
        import scipy.integrate
        ints = scipy.integrate.simps(y, x)
        x, y = x*T, y/T
    else:
        print 'manca il file'
        sys.exit(1)

    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1, xlim=(min(x), max(x)), ylim=(0, max(y)*1.2), autoscale_on=False)
    #p = fig.add_subplot(1, 1, 1)
    #p = fig.add_subplot(1, 1, 1)
    #noopt_line = p.plot(loads, busy_noopt, 'r')
    line = ax.plot(x, y, 'b')
    pyplot.xlabel('$fT$')
    pyplot.ylabel('$Densit\`a \, Spettrale$')
    #pyplot.legend((line,), ('int: %.2f' %ints,))
    fig.savefig('%s.eps' %options.output_file)
